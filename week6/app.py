from flask import Flask, request, render_template, g
import sqlite3
from datetime import datetime
import requests
from flask import redirect,url_for
from config import api_key


app = Flask(__name__)


def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect('stocks.db')
        g.db.row_factory = sqlite3.Row
    return g.db.cursor()

# Set up database connection
with app.app_context():
    db = get_db()
    db.execute('CREATE TABLE IF NOT EXISTS stocks (id INTEGER PRIMARY KEY, symbol TEXT, tracking_price FLOAT, current_price FLOAT, num_shares INTEGER, date_added TEXT, percentage_change FLOAT)')


# Define routes
@app.route('/', methods=['GET'])
def index():
    c = get_db()
    c.execute('SELECT * FROM stocks')
    stocks = c.fetchall()
    return render_template('index.html', stocks=stocks)

@app.route('/stock', methods=['POST'])
def stock():
    symbol = request.form['symbol']
    tracking_price = request.form['tracking_price']
    num_shares = request.form['num_shares']
    date_added = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    url = f'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey={api_key}'
    response = requests.get(url)
    stock_data = response.json()
    current_price = float(stock_data['Global Quote']['05. price'])
    percentage_change = ((current_price - float(tracking_price)) / float(tracking_price)) * 100
    c = get_db()
    c.execute('INSERT INTO stocks (symbol, tracking_price, current_price, num_shares, date_added, percentage_change) VALUES (?, ?, ?, ?, ?, ?)', (symbol, tracking_price, current_price, num_shares, date_added, percentage_change))
    g.db.commit()
    c.execute('SELECT id, symbol, tracking_price, current_price, num_shares, date_added, percentage_change FROM stocks')
    stocks = c.fetchall()
    return render_template('index.html', stocks=stocks)

@app.route('/remove_stock', methods=['POST'])
def remove_stock():
    stock_id = request.form['stock_id']
    c = get_db()
    c.execute('DELETE FROM stocks WHERE id = ?', (stock_id,))
    g.db.commit()
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run()