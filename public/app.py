from flask import Flask, request, render_template
import sqlite3

app = Flask(__name__)

# Set up database connection
conn = sqlite3.connect('stocks.db')
c = conn.cursor()
c.execute('CREATE TABLE IF NOT EXISTS stocks (symbol TEXT)')
conn.commit()

# Define routes
@app.route('/', methods=['GET'])
def index():
    return render_template('index.html', stock_symbol='')

@app.route('/stock', methods=['POST'])
def stock():
    symbol = request.form['symbol']
    c.execute('INSERT INTO stocks (symbol) VALUES (?)', (symbol,))
    conn.commit()
    return render_template('index.html', stock_symbol=symbol)

if __name__ == '__main__':
    app.run()

