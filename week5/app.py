import requests
from flask import Flask, request, render_template
from config import api_key

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        symbol = request.form['symbol']
        overview_url = f'https://www.alphavantage.co/query?function=OVERVIEW&symbol={symbol}&apikey={api_key}'
        intra_url = f'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey={api_key}'
        news_url = f'https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers={symbol}&apikey={api_key}'
        daily_url = f'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={symbol}&apikey={api_key}'
        overview_r = requests.get(overview_url)
        intra_r = requests.get(intra_url)
        news_r = requests.get(news_url)
        daily_r = requests.get(daily_url)
        overview_data = overview_r.json()
        intra_data = intra_r.json()
        news_data = news_r.json()
        daily_data = daily_r.json()

        closing_prices = [float(daily_data['Time Series (Daily)'][date]['4. close']) for date in daily_data['Time Series (Daily)']]
        dates = list(daily_data['Time Series (Daily)'].keys())

        sorted_news = []
        for item in news_data['feed']:
            sorted_news.append(item['title'])
            sorted_news.append(item['summary'])
            sorted_news.append(item['url'])
            

        formatted_news = []
        for i in range(15):
            formatted_news.append(sorted_news[i])
            
        news_output = ""
        for news in formatted_news:
           news_output += news + '\n'

        data = {
            'overview': overview_data,
            'intra': intra_data,
            'news': formatted_news, 
            'daily': daily_data       
        }
        return render_template('display_data.html', data=data, dates = dates, closing_prices = closing_prices )
        
    return '''
        <!DOCTYPE html>
        <html>
          <head>
            <title>Stock Information Search</title>
          </head>
          <body>
            <h1>Stock Information Search</h1>
            <form action="/" method="POST">
              <label for="symbol">Stock Symbol:</label>
              <input type="text" id="symbol" name="symbol">
              <input type="submit" value="Submit">
            </form>
          </body>
        </html>
    '''   
    

if __name__ == '__main__':
    app.run(debug=True)
